import java.util.Arrays;

public class StringSorting {
    public static void main(String[] args) {
        String[] array1 = new String[]{"This", "string task", "is", "really", "weird"};
        String[] array2 = new String[]{"This", "string task", "is", "really", "weird"};
        String[] array3 = new String[]{"This", "string task", "is", "really", "weird"};
        System.out.println(Arrays.toString(array1));
        SelectionSorting.stringSort(array1);
        displaySelection(array1);
        BubbleSotring.stringSort(array2);
        displayBubble(array2);
        InsertionSorting.stringSort(array3);
        displayInsert(array3);
    }
    public static void displaySelection(String[] array1){
        System.out.println("Selection: ");
        for (String element : array1) {
            System.out.print(element + " ");
        }
        System.out.println();
    }
    public static void displayBubble(String[] array2) {
        System.out.println("Bubble: ");
        for (String element : array2) {
            System.out.print(element + " ");
        }
        System.out.println();
    }
    public static void displayInsert(String[] array3){
        System.out.println("Insert: ");
        for (String element : array3) {
            System.out.print(element + " ");
        }
        System.out.println();
    }
}

