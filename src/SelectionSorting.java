public class SelectionSorting {
    public static void stringSort(String[] array1){
        for (int i= 0; i< array1.length; i++){
            int position=i;
            for (int j =i+1; j < array1.length;j++ ){
                if (array1[j].compareTo(array1[position])<=0){
                    position=j;
                }
            }
            String temp = array1[position];
            array1[position]= array1[i];
            array1[i] = temp;
        }

    }
}
