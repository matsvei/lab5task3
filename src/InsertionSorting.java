
public class InsertionSorting {
    public static void stringSort(String[] array3){
        for (int i = 0; i< array3.length; i++){
            String currentElement = array3[i];
            int previousKey = i-1;
            while (previousKey>=0 && array3[previousKey].compareTo(currentElement)>=0){
                array3[previousKey+1]= array3[previousKey];
                array3[previousKey]= currentElement;
                previousKey--;
            }
        }
    }
}